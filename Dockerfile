FROM ruby:2.3

RUN mkdir -p /root/.aws
COPY .aws-credentials /root/.aws/credentials

WORKDIR /usr/src/app
COPY Gemfile* ./
RUN bundle install
COPY . .

EXPOSE 3000
CMD ["rails", "server", "-b", "0.0.0.0"]
