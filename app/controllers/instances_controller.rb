class InstancesController < ApplicationController

  def index
    render  status: 404, json: "These is not the api you are looking for".to_json
  end

  # Creates a new AWS instance and configures a given user & password combination
  # Params:
  # +username+ Username to be used when connecting to the machine
  # +password+ Password to go along with the username
  def create
    user = params[:username]
    pw = params[:password]

    # Only proceed if we have what we need
    self.badRequest("username") and return if user.nil?
    self.badRequest("password") and return if pw.nil?

    begin
      # Create a user and modify AWS defaults to allow ssh access via password
      script = "#! /bin/bash" + "\n adduser --gecos \"\" --disabled-password #{user}" + "\n echo \"#{user}:#{pw}\" | chpasswd"
      script += "\n sed -i '/PasswordAuthentication no/c\\PasswordAuthentication yes' /etc/ssh/sshd_config" + "\n service ssh restart"
      #puts "Script: #{script}"

      encoded_script = Base64.encode64(script)

      ec2 = self.ec2Resource()
      instance = ec2.create_instances({
        image_id: Rails.configuration.instances['image_id'],
        min_count: 1,
        max_count: 1,
        user_data: encoded_script,
        instance_type: Rails.configuration.instances['type'],
        subnet_id: Rails.configuration.instances['subnet_id']
      })

      # Per definition, it is ok to keep the client waiting until the instance is ready
      ec2.client.wait_until(:instance_status_ok, {instance_ids: [instance[0].id]})

      # Get full info about the newly created instance
      # Required to make sure we have the public IP address available in the object
      newInstance = ec2.instance(instance[0].id)
      puts "Instance IP: #{newInstance.public_ip_address}"

      instanceDetails = {
        'id' => newInstance.id,
        'public_ip' => newInstance.public_ip_address,
        'state' => newInstance.state.name,
        'username' => user
      }
      render json: instanceDetails.to_json

    rescue Aws::EC2::Errors::ServiceError
      render json: "500 Internal Server Error".to_json, :status => 500
    end
  end

  def destroy
    instanceId = params[:id]

    begin
      ec2 = self.ec2Resource()
      instance = ec2.instance(instanceId)
      instance.terminate()
      render json: "OK".to_json
    rescue Aws::EC2::Errors::InvalidInstanceIDNotFound
      render :json => "404 Not Found".to_json, :status => 404
    rescue Aws::EC2::Errors::ServiceError
      render :json => "500 Internal Server Error".to_json, :status => 500
    end
  end

  protected

  def ec2Resource
    return Aws::EC2::Resource.new(region: Rails.configuration.instances['region'])
  end

  def badRequest (param)
    render :json => "Bad Request: Missing #{param} parameter".to_json, :status => 400
  end
end


