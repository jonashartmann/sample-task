Rails.application.routes.draw do
  resource :instances, only: [:index, :create, :destroy]

  root 'instances#index'
end

