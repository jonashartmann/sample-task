# SuitePad sample task

This is an API for creating instances in AWS with an arbitrary user and SSH access via password

Built using Ruby 2.3 and Rails 5.0.0

## Configuration
Update the file `config/application.rb` and change the values of the `config.instances` hash to fit your AWS environment.

### Setup your AWS credentials

The SDK will search the credentials in some default places. It is all documented here: http://docs.aws.amazon.com/sdkforruby/api/#Configuration

In case you want to use [docker](https://docs.docker.com/), there is a Dockerfile in this repository. It expects a file called `.aws-credentials` in the root of the project so it can configure the image properly.
Add that file with your AWS credentials, as specified for the shared configuration files in the link above.

The file will then be copied into the image path `.aws/credentials` on build.

## Running with docker


#### Build image

	docker build -t ruby-sample .

#### Run container with that image

	docker run -d -p 8080:3000 --name sample ruby-sample

This will create a container and map the app to the port 8080 on the host. The URL to access it will depend on your operating system and docker version installed. So please consult the [official docker documentation](https://docs.docker.com/) for support.

#### Confirm that the server is running by checking the logs

	docker logs sample

## Running without docker
If you want to run it without docker, make sure you install the dependencies first. Those two commands should be enough if you already have your environment ready.

	bundle install
	rails server

As default, the server listens on port 3000.

## Routes available

	POST   /instances(.:format) instances#create
	DELETE /instances(.:format) instances#destroy
	GET    /                    instances#index

- __instances#create:__ Expects parameters `username` and `password`. It creates a new instance and setup SSH access for the user. It will take a few minutes to complete the request.
- __instances#destroy:__ Expects parameter `id` with the image id to destroy. It terminates the instance with the given ID.
- __instances#index:__ Only for testing that the server is running.

## Notes about the task
- I gave no attention to security, since that would explode the scope of this task
- Testing was also left behind for the same reason
- Most error handling and responses are not ideal JSON API responses, but I had to stop refactoring at a certain point
- Main files to look at:
	- `config/routes.rb`
	- `app/controllers/instances_controller.rb`
- Since it was easy and useful, I've also added an API to delete an instance by id
- I think that a better way to solve this would have been using a custom made AMI that already sets up the correct flags to allow SSH authentication via passwords. But since that would be difficult to align with your side, I've added to the userdata script a part that updates the sshd configuration on the fly.